const LayoutModel = require('../models/layout.model')
const FormModel = require('../models/form.model')
const FieldModel = require('../models/field.model')

const mongoose = require('mongoose')

const catchErr = require('../helper/HandlerError').catchError
const AppError = require('../helper/HandlerError').AppError
const snippet = require('./snippet')

exports.aliasTop = (req, res, next) => {
  req.query.limit = '6'
  return next()
}

exports.readTemplate = catchErr(async (req, res, next) => {
  const result = await LayoutModel
    .find({ owner: 'template' })
    .populate({
      path: 'FormField'
    })
  return res.status(200).json({
    status: 201,
    data: result
  })
})

exports.readOne = snippet.readOne(LayoutModel, { type: 'idLayout' })

exports.readFull = catchErr(async (req, res, next) => {
  const id = req.params.idLayout
  const result = await LayoutModel
    .findById(id)
    .populate({
      path: 'FormField'
    })
  return res.status(200).json({
    status: 201,
    data: result
  })
})

exports.createOne = snippet.createOne(LayoutModel)
exports.createTemplate = snippet.createOne(LayoutModel, { owner: 'template' })
exports.createById = catchErr(async (req, res, next) => {
  const iduser = req.payload._id
  const idForm = req.params.idForm
  const idLayout = req.body.idLayout
  const Layout = await LayoutModel
    .findById(idLayout)
    .populate({
      path: 'FormField'
    })
  if (!Layout) return next(new AppError('Find not found id Layout', 500))

  let docLayout = await LayoutModel.create({ title: Layout.title, description: Layout.description, owner: iduser })
  const IdLayoutNew = docLayout._id
  await FormModel.findByIdAndUpdate(
    idForm,
    { $push: { FormLayout: IdLayoutNew } }
  )

  const ArrField = Layout.FormField
  if (ArrField.length === 0) {
    return res.status(201).json({
      status: 'success',
      data: docLayout
    })
  }
  const ArrFieldNew = []
  for (let i = 0; i < ArrField.length; i++) {
    const Field = ArrField[i]
    Field._id = mongoose.Types.ObjectId()
    Field.__v = undefined
    Field.owner = iduser
    const docField = await FieldModel.create(Field.toObject())
    ArrFieldNew.push(docField._id)
  }

  await LayoutModel.findByIdAndUpdate(
    IdLayoutNew,
    { FormField: ArrFieldNew }
  )
  docLayout = await LayoutModel.findById(IdLayoutNew)
  return res.status(201).json({
    status: 'success',
    data: docLayout
  })
})

exports.updateOne = catchErr(async (req, res, next) => {
  const id = req.params.idLayout
  const updateDoc = req.body
  await LayoutModel.findByIdAndUpdate(
    id,
    updateDoc,
    {
      new: true,
      runValidators: true
    }
  )
  const doc = await LayoutModel
    .findById(id)
    .populate({
      path: 'FormField'
    })
  return res.status(200).json({
    status: 'success',
    data: doc
  })
})

exports.deleteOneLink = catchErr(async (req, res, next) => {
  const idLayout = req.params.idLayout
  const idForm = req.params.idForm

  const Layout = await LayoutModel.findById(idLayout)

  // delete field of layout
  const ArrField = Layout.FormField
  for (let i = 0; i < ArrField.length; i++) {
    await FieldModel.findByIdAndDelete(ArrField[i])
  }
  // delete layout
  await LayoutModel.findByIdAndDelete(idLayout)
  // update form
  await FormModel.update(
    { _id: idForm },
    { $pull: { FormLayout: idLayout } }
  )
  return res.status(201).json({
    status: 'success',
    data: null
  })
})

exports.deleteOne = snippet.deleteOne(LayoutModel, { type: 'idLayout' })
