const APIFeatures = require('./apiFeatures')
const catchError = require('../helper/HandlerError').catchError

exports.updateOne = (Model, option = { type: 'none' }) =>
  catchError(async (req, res, next) => {
    // handler type id | default id
    let id = ''
    switch (option.type) {
      case 'none':
        id = req.params.id
        break
      case 'idForm':
        id = req.params.idForm
        break
      case 'idLayout':
        id = req.params.idLayout
        break
      case 'idField':
        id = req.params.idField
        break
      default:
        break
    }
    const updateDoc = req.body
    const doc = await Model.findByIdAndUpdate(
      id,
      updateDoc,
      {
        new: true,
        runValidators: true
      }
    )
    return res.status(200).json({
      status: 'success',
      data: doc
    })
  })

exports.createOne = (Model, option = { owner: 'none' }) =>
  catchError(async (req, res, next) => {
    const doc = { ...req.body }
    switch (option.owner) {
      case 'template':
        doc.owner = 'template'
        break
      default:
        break
    }
    const docRes = await Model.create(doc)
    return res.status(201).json({
      status: 'success',
      data: docRes
    })
  })

exports.readOne = (Model, option = { type: 'none' }) =>
  catchError(async (req, res, next) => {
    // handler type id | default id
    let id = ''
    switch (option.type) {
      case 'none':
        id = req.params.id
        break
      case 'idForm':
        id = req.params.idForm
        break
      case 'idLayout':
        id = req.params.idLayout
        break
      case 'idField':
        id = req.params.idField
        break
      case 'idData':
        id = req.params.idData
        break
      default:
        break
    }
    const doc = await Model.findById(id)
    return res.status(200).json({
      status: 'success',
      data: doc
    })
  })

exports.readAll = (Model) =>
  catchError(async (req, res, next) => {
    const features = new APIFeatures(Model.find({}), req.query)
      .filter()
      .sort()
      .limitFields()
      .paginate()
    const doc = await features.query

    return res.status(200).json({
      status: 'success',
      results: doc.length,
      data: doc
    })
  })

exports.deleteOne = (Model, option = { type: 'none' }) =>
  catchError(async (req, res, next) => {
    // handler type id | default id
    let id = ''
    switch (option.type) {
      case 'none':
        id = req.params.id
        break
      case 'idForm':
        id = req.params.idForm
        break
      case 'idLayout':
        id = req.params.idLayout
        break
      case 'idField':
        id = req.params.idField
        break
      case 'idData':
        id = req.params.idData
        break
      default:
        break
    }
    await Model.findByIdAndDelete(id)
    return res.status(205).json({
      status: 'success',
      data: null
    })
  })
