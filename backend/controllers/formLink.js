/**
 * trả về đủ dữ liệu của form
 */

const FormModel = require('../models/form.model')
require('../models/layout.model')
require('../models/field.model')

const catchErr = require('../helper/HandlerError').catchError

exports.index = catchErr(async (req, res, next) => {
  const idForm = req.params.idForm
  const result = await FormModel
    .findById(idForm)
    .populate({
      path: 'FormLayout',
      populate: {
        path: 'FormField'
      }
    })
  return res.status(200).json({
    status: 201,
    data: result
  })
})
