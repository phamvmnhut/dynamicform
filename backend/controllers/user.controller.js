exports.readOne = (req, res, next) => {
  const user = req.payload
  return res.status(204).json({
    status: 'success',
    data: user
  })
}
// exports.updateOne = snippet.updateOne(UserModel)
// exports.deleteOne = snippet.deleteOne(UserModel)

/**
 * chứng thực cái token đó, và lấy ra thông tin.
 * sử dụng thông tin đó để render ra user thông tin cần thiết,
 */
