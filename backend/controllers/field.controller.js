const FieldModel = require('../models/field.model')
const LayoutModel = require('../models/layout.model')

const mongoose = require('mongoose')

const catchErr = require('../helper/HandlerError').catchError
const AppError = require('../helper/HandlerError').AppError

const snippet = require('./snippet')

exports.aliasTop = (req, res, next) => {
  req.query.limit = '6'
  return next()
}

exports.readTemplate = catchErr(async (req, res, next) => {
  const result = await FieldModel
    .find({ owner: 'template' })
  return res.status(200).json({
    status: 201,
    data: result
  })
})

exports.readOne = snippet.readOne(FieldModel, { type: 'idField' })
exports.createTemplate = snippet.createOne(FieldModel, { owner: 'template' })
exports.createOne = snippet.createOne(FieldModel)

exports.createById = catchErr(async (req, res, next) => {
  const iduser = req.payload._id
  const idLayout = req.params.idLayout
  const idField = req.body.idField
  const Field = await FieldModel.findById(idField)
  if (!Field) return next(new AppError('find not found id Field', 500))

  Field._id = mongoose.Types.ObjectId()
  Field.__v = undefined
  Field.owner = iduser
  const docField = await FieldModel.create(Field.toObject())

  await LayoutModel.update(
    { _id: idLayout },
    { $push: { FormField: docField._id } }
  )
  return res.status(201).json({
    status: 'success',
    data: docField
  })
})

exports.updateOne = snippet.updateOne(FieldModel, { type: 'idField' })

exports.deleteOneLink = catchErr(async (req, res, next) => {
  const idLayout = req.params.idLayout
  const idField = req.params.idField
  await FieldModel.findByIdAndDelete(idField)
  await LayoutModel.updateOne(
    { _id: idLayout },
    { $pull: { FormField: idField } }
  )
  return res.status(201).json({
    status: 'success',
    data: null
  })
})

exports.deleteOne = snippet.deleteOne(FieldModel, { type: 'idField' })
