const FormModel = require('../models/form.model')
const LayoutModel = require('../models/layout.model')
const FieldModel = require('../models/field.model')
const DataModel = require('../models/Data.model')

const mongoose = require('mongoose')

const catchError = require('../helper/HandlerError').catchError
const AppError = require('../helper/HandlerError').AppError
const APIFeatures = require('./apiFeatures')

const snippet = require('./snippet')

exports.aliasTop = (req, res, next) => {
  req.query.limit = '5'
  return next()
}

exports.readAllByUserID = catchError(async (req, res, next) => {
  const idUser = req.payload._id
  const features = new APIFeatures(FormModel.find({ owner: idUser }), req.query)
    .filter()
    .sort()
    .limitFields()
    .paginate()
  const doc = await features.query
  return res.status(200).json({
    status: 'success',
    results: doc.length,
    data: doc
  })
})

exports.readTemplate = catchError(async (req, res, next) => {
  const result = await FormModel
    .find({ owner: 'template' })
    .populate({
      path: 'FormLayout'
    })
  return res.status(200).json({
    status: 201,
    data: result
  })
})

exports.readOne = snippet.readOne(FormModel, { type: 'idForm' })

exports.createOne = snippet.createOne(FormModel)
exports.createTemplate = snippet.createOne(FormModel, { owner: 'template' })
exports.createById = catchError(async (req, res, next) => {
  const iduser = req.payload._id
  const idForm = req.body.idForm
  const Form = await FormModel
    .findById(idForm)
    .populate({
      path: 'FormLayout',
      populate: {
        path: 'FormField'
      }
    })
  if (!Form) return next(new AppError('find not found id form', 500))
  const docForm = await FormModel.create({ title: Form.title, description: Form.description, owner: iduser })
  const ArrLayout = Form.FormLayout
  if (ArrLayout.length === 0) {
    return res.status(201).json({
      status: 'success',
      data: docForm
    })
  }

  const ArrLayoutNew = []
  for (let i = 0; i < ArrLayout.length; i++) {
    const Layout = ArrLayout[i]
    const docLayout = await LayoutModel.create({ title: Layout.title, description: Layout.description, owner: iduser })
    const ArrField = Layout.FormField
    if (ArrField.length === 0) continue

    const ArrFieldNew = []
    for (let j = 0; j < ArrField.length; j++) {
      const Field = ArrField[j]
      Field._id = mongoose.Types.ObjectId()
      Field.__v = undefined
      Field.owner = iduser
      const docField = await FieldModel.create(Field.toObject())
      ArrFieldNew.push(docField._id)
    }
    await LayoutModel.findByIdAndUpdate(
      docLayout._id,
      { FormField: ArrFieldNew }
    )
    ArrLayoutNew.push(docLayout._id)
  }
  await FormModel.findByIdAndUpdate(
    docForm._id,
    { FormLayout: ArrLayoutNew }
  )
  const result = await FormModel.findById(docForm._id)
  return res.status(201).json({
    status: 'success',
    data: result
  })
})

/**
 * update cho phép thay đổi thứ tự của layout,
 * chưa fix việc xoá và thêm tự động field của người khác
 */
exports.updateOne = snippet.updateOne(FormModel, { type: 'idForm' })

exports.deleteOneLink = catchError(async (req, res, next) => {
  const idForm = req.params.idForm
  const Form = await FormModel
    .findById(idForm)
    .populate({
      path: 'FormLayout',
      populate: {
        path: 'FormField'
      }
    })
  const ArrLayout = Form.FormLayout
  for (let i = 0; i < ArrLayout.length; i++) {
    // delete field
    const ArrField = ArrLayout[i].FormField
    for (let j = 0; j < ArrField.length; j++) {
      await FieldModel.findByIdAndDelete(ArrField[j]._id)
    }
    // delete Layout
    await LayoutModel.findByIdAndDelete(ArrLayout[i]._id)
  }
  // delete form
  await FormModel.findByIdAndDelete(Form._id)

  // delete data submit
  await DataModel.deleteMany({ idform: Form._id })

  return res.status(201).json({
    status: 'success',
    data: null
  })
})

exports.deleteOne = snippet.deleteOne(FormModel, { type: 'idForm' })
