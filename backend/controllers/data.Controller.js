const DataModel = require('../models/Data.model')
const FormModel = require('../models/form.model')

const catchError = require('../helper/HandlerError').catchError
const AppError = require('../helper/HandlerError').AppError
const APIFeatures = require('./apiFeatures')

const snippet = require('./snippet')

exports.readAllByForm = catchError(async (req, res, next) => {
  const idUser = req.payload._id
  // check form thuộc user hay không
  const formID = req.query.idform
  if (formID === undefined) return next(new AppError('you dont have formID', 500))
  const form = await FormModel.findById(formID)
  if (!form) return next(new AppError('find not found id form', 500))
  if (form.owner !== idUser) return next(new AppError('you dont have permission', 403))

  const features = new APIFeatures(DataModel.find(), req.query)
    .filter()
    .sort()
    .limitFields()
    .paginate()
  const doc = await features.query
  return res.status(200).json({
    status: 'success',
    results: doc.length,
    data: doc
  })
})
exports.readOne = snippet.readOne(DataModel, { type: 'idData' })

exports.createOne = snippet.createOne(DataModel)

exports.deleteOne = snippet.deleteOne(DataModel, { type: 'idData' })

exports.deleteOnePer = catchError(async (req, res, next) => {
  const idUser = req.payload._id

  const idData = req.params.idData
  const data = await DataModel.findById(idData)
  if (!data) return next(new AppError('find not found data', 500))

  const formID = data.toObject().idform
  if (formID === undefined) return next(new AppError('data dont have formID', 500))
  const form = await FormModel.findById(formID)
  if (!form) return next(new AppError('find not found id form', 500))
  if (form.owner !== idUser) return next(new AppError('you dont have permission', 403))

  await DataModel.findByIdAndDelete(idData)
  return res.status(200).json({
    status: 'success',
    data: null
  })
})
