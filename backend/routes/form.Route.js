var express = require('express')
var router = express.Router()
const authMidd = require('../helper/auth').isAuth

const formCtrl = require('../controllers/form.controller')
const layoutCtrl = require('../controllers/Layout.controller')
const fieldCtrl = require('../controllers/field.controller')

router.use(authMidd)

router.route('/')
  .get(formCtrl.aliasTop, formCtrl.readAllByUserID)
  .post(formCtrl.createById)
router.route('/:idForm')
  .get(formCtrl.readOne)
  .patch(formCtrl.updateOne)
  .delete(formCtrl.deleteOneLink)

router.route('/:idForm/layout/')
  .get(layoutCtrl.readTemplate)
  .post(layoutCtrl.createById)
router.route('/:idForm/layout/:idLayout')
  .get(layoutCtrl.readOne)
  .patch(layoutCtrl.updateOne)
  .delete(layoutCtrl.deleteOneLink)
router.route('/:idForm/layout/:idLayout/full')
  .get(layoutCtrl.readFull)

router.route('/:idForm/layout/:idLayout/field/')
  .get(fieldCtrl.readTemplate)
  .post(fieldCtrl.createById)
router.route('/:idForm/layout/:idLayout/field/:idField')
  .get(fieldCtrl.readOne)
  .patch(fieldCtrl.updateOne)
  .delete(fieldCtrl.deleteOneLink)

module.exports = router
