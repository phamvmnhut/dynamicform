var express = require('express')
var router = express.Router()

router.route('/')
  .get((req, res, next) => { res.send('GET index /') })
module.exports = router
