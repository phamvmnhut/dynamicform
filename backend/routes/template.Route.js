var express = require('express')
var router = express.Router()
const authMidd = require('../helper/auth').isAuth

const formCtrl = require('../controllers/form.controller')
const layoutCtrl = require('../controllers/Layout.controller')
const fieldCtrl = require('../controllers/field.controller')

router.use(authMidd)

router.route('/form')
  .get(formCtrl.readTemplate)

router.route('/layout')
  .get(layoutCtrl.readTemplate)

router.route('/field')
  .get(fieldCtrl.readTemplate)

module.exports = router
