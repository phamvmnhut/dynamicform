var express = require('express')
var router = express.Router()
var authMid = require('../helper/auth').isAuth
var userCtrl = require('../controllers/user.controller')

router.use(authMid)
router.route('/')
  .get(userCtrl.readOne)

module.exports = router
