var express = require('express')
var router = express.Router()

const formCtrl = require('../controllers/form.controller')
const layoutCtrl = require('../controllers/Layout.controller')
const fieldCtrl = require('../controllers/field.controller')

router.route('/form')
  .post(formCtrl.createTemplate)
router.route('/form/:idForm')
  .get(formCtrl.readOne)
  .patch(formCtrl.updateOne)
  .delete(formCtrl.deleteOne)

router.route('/layout')
  .post(layoutCtrl.createTemplate)
router.route('/layout/:idLayout')
  .get(layoutCtrl.readOne)
  .patch(layoutCtrl.updateOne)
  .delete(layoutCtrl.deleteOne)

router.route('/field')
  .post(fieldCtrl.createTemplate)
router.route('/field/:idField')
  .get(fieldCtrl.readOne)
  .patch(fieldCtrl.updateOne)
  .delete(fieldCtrl.deleteOne)
module.exports = router
