var express = require('express')
var router = express.Router()
var authMid = require('../helper/auth').isAuth
var dataCtrl = require('../controllers/data.controller')

router.route('/')
  .post(dataCtrl.createOne)

router.use(authMid)
router.route('/')
  .get(dataCtrl.readAllByForm)

router.route('/:idData')
  .delete(dataCtrl.deleteOnePer)

module.exports = router
