class AppError extends Error {
  constructor (message, statusCode) {
    super(message)

    this.statusCode = statusCode
    this.status = `${statusCode}`.startsWith('4') ? 'fail' : 'error'
    this.isOperational = true

    Error.captureStackTrace(this, this.constructor)
  }
}

function catchError (fn) {
  return (req, res, next) => { fn(req, res, next).catch(next) }
};

const handleCastErrorDB = (err) => {
  const message = `Invalid ${err.path}: ${err.value}`
  return new AppError(message, 400)
}
const handleDuplicateFieldsDB = (err) => {
  const value = err.errmsg.match(/(["'])(?:\\.|[^\\])*?\1/)
  const message = `${value} is already existed`
  return new AppError(message, 400)
}
const handleValidatorErrorDB = (err) => {
  const errors = Object.values(err.errors).map((el) => el.message)
  const message = `Invalid input data: ${errors.join('. ')}`
  return new AppError(message, 400)
}

const sendErrorDev = (err, res) => {
  res.status(err.statusCode).json({
    status: err.status,
    error: err,
    message: err.message,
    stack: err.stack
  })
}
const sendErrorProd = (err, res) => {
  // Operational Error
  if (err.isOperational) {
    res.status(err.statusCode).json({
      status: err.status,
      message: err.message
    })
  } else { // Unknown Error
    console.error('ERROR:0:', err)
    res.status(500).json({
      status: 'error',
      message: 'Something went wrong!'
    })
  }
}
const AppHandleErr = (err, req, res, next) => {
  err.statusCode = err.statusCode || 500
  err.status = err.status || 'error'

  if (process.env.NODE_ENV === 'development') {
    sendErrorDev(err, res)
  } else if (process.env.NODE_ENV === 'production') {
    let error = { ...err }
    if (error.name === 'CastError') {
      error = handleCastErrorDB(error)
      sendErrorProd(error, res)
    }
    if (err.code === 11000) {
      error = handleDuplicateFieldsDB(error)
      sendErrorProd(error, res)
    }
    if (error.name === 'ValidationError') {
      error = handleValidatorErrorDB(error)
      sendErrorProd(error, res)
    }

    sendErrorProd(err, res)
  }
}

module.exports = {
  AppError,
  catchError,
  AppHandleErr
}
