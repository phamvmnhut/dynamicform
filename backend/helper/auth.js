const jwt = require('jsonwebtoken')

const accessTokenSecret = process.env.ACCESSTOKENSECRET

const AppErr = require('../helper/HandlerError').AppError

const verifyToken = (token, secretKey) => {
  return new Promise((resolve, reject) => {
    jwt.verify(token, secretKey, (error, decoded) => {
      if (error) {
        return reject(error)
      }
      return resolve(decoded)
    })
  })
}
/**
 * đổi tên lại 2 hàm để sử dụng
 */
module.exports.Auth = async (req, res, next) => {
  const tokenFromClient = req.body.token || req.query.token || req.headers['x-access-token']
  if (tokenFromClient) {
    try {
      const decoded = await verifyToken(tokenFromClient, accessTokenSecret)
      req.payload = decoded
      return next()
    } catch (error) {
      return next(new AppErr('Unauthorized', 401))
    }
  } else {
    return next(new AppErr('No token provided', 403))
  }
}

module.exports.isAuth = async (req, res, next) => {
  req.payload = {
    _id: 'abc',
    username: 'PhamNhut',
    phoneNumber: '0123456789'
  }
  return next()
}
