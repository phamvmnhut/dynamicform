const mongoose = require('mongoose')
const Schema = mongoose.Schema

var FieldSchema = new Schema({
  type: String,
  component: {
    type: String,
    required: [true, 'Field must is component']
  },
  owner: String,
  title: String
}, { strict: false })

module.exports = mongoose.model('Field', FieldSchema)
