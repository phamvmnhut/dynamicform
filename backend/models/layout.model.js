const mongoose = require('mongoose')
const Schema = mongoose.Schema

var LayoutSchema = new Schema({
  title: {
    type: String,
    require: [true, 'Title of Page Layout is require']
  },
  description: String,
  owner: String,
  FormField: [{
    type: Schema.Types.ObjectId,
    ref: 'Field'
  }] // để đặt vị trí trước sau giữa các field
},
{ timestamps: true }
)

module.exports = mongoose.model('Layout', LayoutSchema)
