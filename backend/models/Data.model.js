const mongoose = require('mongoose')
const Schema = mongoose.Schema

var DataSchema = new Schema({
  formid: {
    type: Schema.Types.ObjectId,
    ref: 'Form'
  },
  layoutid: {
    type: Schema.Types.ObjectId,
    ref: 'Layout'
  }
}, { strict: false })

module.exports = mongoose.model('Data', DataSchema)
