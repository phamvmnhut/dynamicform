const mongoose = require('mongoose')
const Schema = mongoose.Schema

var StatusFormSchema = new Schema({
  StatusName: String
})

module.exports = mongoose.model('StatusForm', StatusFormSchema)
