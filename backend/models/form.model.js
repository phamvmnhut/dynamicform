const mongoose = require('mongoose')
const Schema = mongoose.Schema

// ghép các layout lại
var formSchema = new Schema({
  title: {
    type: String,
    required: [true, 'Title Form is require']
  },
  description: String,
  status: {
    type: Schema.Types.ObjectId,
    ref: 'StatusForm'
  },
  FormLayout: [{
    type: Schema.Types.ObjectId,
    ref: 'Layout'
  }], // để đặt vị trí trước sau giữa các layout
  owner: String,
  collaborator: [{
    type: Schema.Types.ObjectId,
    ref: 'User'
  }]
},
{ timestamps: true }
)

module.exports = mongoose.model('Form', formSchema)
