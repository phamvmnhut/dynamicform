const express = require('express')
const app = express()
const path = require('path')
require('dotenv').config()
var morgan = require('morgan')
var rfs = require('rotating-file-stream')
var helmet = require('helmet')
var mongoose = require('mongoose')
var bodyParser = require('body-parser')
var cors = require('cors')

// log only 4xx and 5xx responses to console
// Development logging
if (process.env.NODE_ENV === 'development') {
  app.use(morgan('dev', {
    skip: function (req, res) { return res.statusCode < 400 }
  }))
}
// create a rotating write stream
var accessLogStream = rfs.createStream('access.log', {
  interval: '7d', // rotate daily
  path: path.join(__dirname, 'log')
})
// setup the loggers
app.use(morgan('combined', { stream: accessLogStream }))

app.use(cors())
app.use(helmet())
app.disable('x-powered-by')

const mongooseOption = {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useCreateIndex: true,
  useFindAndModify: false
}
mongoose
  .connect(
    process.env.DB_CONNECT || 'mongodb://localhost:27017/dynamicform',
    mongooseOption
  )
  .then(() => {
    console.log('Connect to database')
  })
  .catch((err) => {
    console.log(err.message)
  })

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))
// parse application/json
app.use(bodyParser.json())

// route
const indexRoute = require('./routes/index.Route')
const userRoute = require('./routes/user.Route')
const formRoute = require('./routes/form.Route')
const dataRoute = require('./routes/data.Route')
const templateRoute = require('./routes/template.Route')
const LinkForm = require('./controllers/formLink').index
const adminRoute = require('./routes/admin.Route')

// static file | localhost:<PORT>/static/<file>
app.use('/static', express.static(path.join(__dirname, 'public')))

app.all('/check-healthy', (req, res) => res.status(200).send('Good route !'))

app.use('/api', indexRoute)
app.use('/api/user', userRoute)
app.use('/api/form', formRoute)
app.use('/api/data', dataRoute)
app.use('/api/template', templateRoute)
app.get('/api/link/:idForm', LinkForm)
app.use('/api/admin', adminRoute)

// other route
app.all('*', express.static(path.join(__dirname, '..', 'frontend/build')))

// handler error
const AppHandleErr = require('./helper/HandlerError').AppHandleErr
app.use(AppHandleErr)

app.listen(process.env.PORT, () => console.log(`Server is running in port: ${process.env.PORT}`))
