import React, { Component } from 'react'

import {connect} from 'react-redux'
import * as FieldAction from '../../../Redux/Action/field'

class delField extends Component {
    constructor(props) {
        super(props)
        this.onDelField = this.onDelField.bind(this)
    }
    onDelField = () => {
        const IdForm = this.props.form._id
        const IdLayout = this.props.layout._id
        const index = this.props.index
        const _id = this.props.layout.FormField[index]._id
        this.props.deleteField(IdForm, IdLayout, _id)
        this.props.closePopup()
    }
    render() {
        return (
            <div className='popup_'>
                <div className='popup__inner'>
                    <span>Do you sure to delete this field ?</span>
                    <div >
                        <button onClick={this.onDelField}>yes</button>
                        <button onClick={this.props.closePopup}>no</button>
                    </div>
                </div>  
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        form: state.form,
        layout: state.layout
    }
}
const mapDispatchToProps = (dispatch, props) => {
    return {
        deleteField : (idForm, idLayout, _id) => {
            dispatch(FieldAction.deleteField(idForm, idLayout, _id))
        }
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(delField)