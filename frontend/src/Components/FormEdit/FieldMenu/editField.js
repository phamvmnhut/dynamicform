import React, { Component } from 'react'

import {connect} from 'react-redux'
import * as FieldAction from '../../../Redux/Action/field'

class editField extends Component {
    constructor(props) {
        super(props)
        this.state = {
            data : []
        }
        this.onEditField = this.onEditField.bind(this)
        this.updateInputValue = this.updateInputValue.bind(this)
    }
    onEditField = () => {
        const IdForm = this.props.form._id
        const idLayout = this.props.layout._id
        const index = this.props.index
        const _id = this.props.layout.FormField[index]._id
        const { data } = this.state

        const doc = this.ArrJsonToField(data)
        this.props.updateField(IdForm, idLayout, _id, doc)
        this.props.closePopup()
    }
    ArrJsonToField(ArrJson) {
        let Json = {}
        for (let i = 0; i<ArrJson.length; i++) {
            Json[ArrJson[i].key] = ArrJson[i].value
        }
        return Json
    }
    FieldToArrJson(Field) {
        let x 
        let Arr = []
        for (x in Field) {
            if (x === 'component' || x === '_id' || x === 'owner' || x === '__v' || x === 'type') continue
            let temp = {
                "key": x,
                "value": Field[x] 
            }
            Arr.push(temp)
        }
        return Arr
    }
    updateInputValue = (evt, i) => {
        let NewArr = this.state.data
        NewArr[i].value = evt.target.value
        this.setState({
            data: NewArr
        })
    }
    componentDidMount() {
        const FieldHere = this.props.layout.FormField[this.props.index] || {}
        const Arr = this.FieldToArrJson(FieldHere)
        this.setState({
            data : Arr
        })
    }
    
    render() {
        const JsonFieldHere = this.state.data
        return (
            <div className='popup_'>
                <div className='popup__inner'>
                    <table>
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Edit</th>
                            </tr>
                        </thead>
                        <tbody>
                            {JsonFieldHere.map((e, i) => (
                                <tr key={i}>
                                    <td>
                                        {e.key}
                                    </td>
                                    <td>
                                        <input type="text" value={this.state.data[i].value} onChange={(evt) => this.updateInputValue(evt, i)} />
                                    </td>
                                </tr>
                            ))}
                        </tbody>
                        <tfoot>
                            <tr>
                                <td>End Field</td>
                                <td>---</td>
                            </tr>
                        </tfoot>
                    </table>
                    <div >
                        <button onClick={this.onEditField}>save</button>
                        <button onClick={this.props.closePopup}>close</button>
                    </div>
                </div>  
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        form: state.form,
        layout: state.layout
    }
}
const mapDispatchToProps = (dispatch, props) => {
    return {
        updateField : (idForm, IdLayout, _id, data) => {
            dispatch(FieldAction.updateField(idForm, IdLayout, _id, data))
        }
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(editField)