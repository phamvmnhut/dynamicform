import React, { Component } from 'react'

import {connect} from 'react-redux'
import * as FieldAction from '../../../Redux/Action/field'

class addField extends Component {
    onAddField () {
        const IdForm = this.props.form._id
        const IdLayout = this.props.layout._id
        const _id = this.props._id
        this.props.addField(IdForm, IdLayout, _id)
    }
    render() {
        return (
            <button className="btn_add_field" onClick={()=>this.onAddField()}>+</button>
        )
    }
}

const mapStateToProps = state => {
    return {
        form: state.form,
        layout: state.layout
    }
}
const mapDispatchToProps = (dispatch, props) => {
    return {
        addField : (idForm, idLayout, _id) => {
            dispatch(FieldAction.addField(idForm, idLayout, _id))
        }
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(addField)