import React, { Component } from 'react'

import {connect} from 'react-redux'

import { AutoForm } from 'uniforms-semantic';
import {LayoutToForm} from '../../Services/helper'

import {dataService} from '../../Services'

class ViewForm extends Component {
    onSubmit = (model) => {
        const data = {...model, "idform": this.props.idForm}
        console.log(data)
        dataService.submit(data)
            .then(res => {
                alert('Submit success')
            })
            .catch(err => {
                console.log(err)
                alert('Submit fail')
            })
    }
    onViewData = (model) => {
        alert(JSON.stringify(model, null, 2))
    }
    onPre = ()=> {this.props.previous()}

    onNext = ()=> {this.props.next()}

    render() {
        const { layout } = this.props
        if (layout._id === undefined) {
            return (
                <div className="wrap_viewForm">
                    <div>Select layout</div>
                </div>
            )
        } else {
            const bridge = LayoutToForm(layout)
            const mode = this.props.mode || 0
            let formRef;
            return (
                <div className="wrap_viewForm">
                    <h1>{layout.title}</h1>
                    <p>{layout.description}</p>
                    <AutoForm 
                        ref={ref => (formRef = ref)}
                        schema={bridge}
                        onSubmit={model => this.onSubmit(model)}
                    />
                    { // design form
                        mode === 0 &&
                        <div className="directer">
                            <button onClick={() => formRef.reset()}>Reset Data</button>
                        </div>
                    }
                    { // cur form
                        mode === 1 &&
                        <div className="directer">
                            <button onClick={this.onPre}>Pre Form</button>
                            <button onClick={() => formRef.reset()}>Reset Data</button>
                            <button onClick={this.onNext}>Next Form</button>
                        </div>
                    }
                    { // submit form
                        mode === 2 && 
                        <div className="directer">
                            <button onClick={() => alert('Dang phat trien')}>View Data</button>
                            <button onClick={() => formRef.onSubmit()}>Submit Form</button>
                        </div>
                    }
                </div>
            )
        }
    }
}

const mapStateToProps = (state) => {
    return {
        layout: state.layout
    }
}
const mapDispatchToProps = (dispatch, props) => {
    return {
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ViewForm) 