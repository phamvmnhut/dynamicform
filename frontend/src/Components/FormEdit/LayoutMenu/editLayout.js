import React, { Component } from 'react'

import {connect} from 'react-redux'
import * as LayoutAction from '../../../Redux/Action/layout'

class editLayout extends Component {
    constructor(props) {
        super(props)
        this.state = {
            title : "",
            description: ""
        }
        this.onEditLayout = this.onEditLayout.bind(this)
    }
    onEditLayout = () => {
        const IdForm = this.props.form._id
        const _id = this.props.layout._id
        const { title, description } = this.state
        const data = { title, description }
        
        this.props.updateLayout(IdForm, _id, data)
        this.props.closePopup()
    }
    componentDidMount() {
        const LayoutHere = this.props.layout || {}
        this.setState({
            title : LayoutHere.title,
            description: LayoutHere.description
        })
    }
    
    render() {
        return (
            <div className='popup_'>
                <div className='popup__inner'>
                    <table>
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Edit</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>title</td>
                                <td>
                                    <input
                                        value={this.state.title}
                                        onChange={(e) => this.setState({ title: e.target.value })}>
                                    </input>
                                </td>
                            </tr>
                            <tr>
                                <td>description</td>
                                <td>
                                    <textarea
                                        value={this.state.description}
                                        onChange={(e) => this.setState({ description: e.target.value })}>
                                    </textarea>
                                </td>
                            </tr>
                        </tbody>
                        <tfoot>
                            <tr>
                                <td>End Field</td>
                                <td>---</td>
                            </tr>
                        </tfoot>
                    </table>
                    <div >
                        <button onClick={this.onEditLayout}>save</button>
                        <button onClick={this.props.closePopup}>close</button>
                    </div>
                    
                </div>  
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        form: state.form,
        layout: state.layout
    }
}
const mapDispatchToProps = (dispatch, props) => {
    return {
        updateLayout : (idForm, _id, data) => {
            dispatch(LayoutAction.updateLayout(idForm, _id, data))
        }
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(editLayout)