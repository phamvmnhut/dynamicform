import React, { Component } from 'react'

import {connect} from 'react-redux'
import * as LayoutAction from '../../../Redux/Action/layout'

class editLayout extends Component {
    constructor(props) {
        super(props)
        this.state = {
            FormField : []
        }
        this.onRangeLayout = this.onRangeLayout.bind(this)
        this.up = this.up.bind(this)
        this.down = this.down.bind(this)
    }
    onRangeLayout = () => {
        const IdForm = this.props.form._id
        const _id = this.props.layout._id
        const { FormField } = this.state
        let arr = []
        for (let i = 0; i <FormField.length ;i++){
            arr.push(FormField[i]._id)
        }
        const data = {"FormField": arr}
        this.props.updateLayout(IdForm, _id, data)
        this.props.closePopup()
    }
    componentDidMount() {
        const LayoutHere = this.props.layout || {}
        this.setState({
            FormField : LayoutHere.FormField
        })
    }

    up = (i) => {
        if (i < 1) return
        const arr = this.state.FormField
        var element = arr[i];
        arr.splice(i, 1);
        arr.splice(i-1, 0, element);
        this.setState({
            FormField: arr
        })
    }
    down = (i) => {
        const arr = this.state.FormField
        if (i > arr.length -2) return
        var element = arr[i];
        arr.splice(i, 1);
        arr.splice(i + 1, 0, element);
        this.setState({
            FormField: arr
        })
    }
    render() {
        if (this.state.FormField.length === 0){
            return (
                <div className='popup_'>
                    <div className="popup__inner">
                        <p>Layout dont have any field</p>
                        <button onClick={this.props.closePopup}>close</button>
                    </div>
                </div>
            )
        }

        const {FormField} = this.state
        return (
            <div className='popup_'>
                <div className='popup__inner'>
                    <table>
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>ID</th>
                                <th>Range</th>
                            </tr>
                        </thead>
                        <tbody>

                            {
                                FormField.map((e,i)=> (
                                    <tr key={i}>
                                        <td>{e.titleDis}</td>
                                        <td>{e._id}</td>
                                        <td>
                                            <button onClick={() => this.up(i)}>up</button>
                                            <button onClick={() => this.down(i)}>down</button>
                                        </td>
                                    </tr>
                                ))
                            }
                        </tbody>
                        <tfoot>
                            <tr>
                                <td>End Field</td>
                                <td>---</td>
                                <td>---</td>
                            </tr>
                        </tfoot>
                    </table>
                    <div >
                        <button onClick={this.onRangeLayout}>save</button>
                        <button onClick={this.props.closePopup}>close</button>
                    </div>
                    
                </div>  
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        form: state.form,
        layout: state.layout
    }
}
const mapDispatchToProps = (dispatch, props) => {
    return {
        updateLayout : (idForm, _id, data) => {
            dispatch(LayoutAction.updateLayout(idForm, _id, data))
        }
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(editLayout)