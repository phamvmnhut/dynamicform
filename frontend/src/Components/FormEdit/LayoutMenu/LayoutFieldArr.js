import React, { Component } from 'react'

import {connect} from 'react-redux'
import DelField from '../FieldMenu/delField'
import EditField from '../FieldMenu/editField'

class LayoutFieldArr extends Component {
    constructor(props) {
        super(props)
        this.state = {
            selecField : 0,
            showDelField : false,
            showEditField : false
        }
        this.togglePopupDelField = this.togglePopupDelField.bind(this)
        this.togglePopupEditField = this.togglePopupEditField.bind(this)
    }
    togglePopupDelField = (i) => { this.setState({showDelField: !this.state.showDelField, selecField: i })}
    togglePopupEditField = (i) => { this.setState({showEditField: !this.state.showEditField, selecField: i})}
    
    render() {
        const ListFieldHere = this.props.layout.FormField
        if (ListFieldHere === undefined) return (<div />)

        return (
            <div>
                <table>
                    <tbody>
                        {ListFieldHere.map((e, i) => (
                            <tr key={i}>
                                <td>
                                    <span>{e.titleDis}</span>
                                </td>
                                <td>
                                    <div className="fa fa-wrench" onClick={() => this.togglePopupEditField(i)} />
                                </td>
                                <td>
                                    <div className="fa fa-minus-circle" onClick={() => this.togglePopupDelField(i)} />
                                </td>
                            </tr>
                        ))}
                    </tbody>
                </table>
                {
                    this.state.showEditField &&
                    <EditField index={this.state.selecField} closePopup={this.togglePopupEditField} />
                }
                {
                    this.state.showDelField &&
                    <DelField index={this.state.selecField} closePopup={this.togglePopupDelField} />
                }
            </div>
        )
    }
}


const mapStateToProps = state => {
    return {
        layout: state.layout
    }
}
const mapDispatchToProps = (dispatch, props) => {
    return {
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(LayoutFieldArr)