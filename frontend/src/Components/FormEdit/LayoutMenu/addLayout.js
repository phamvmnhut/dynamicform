import React, { Component } from 'react'

import {connect} from 'react-redux'
import * as LayoutAction from '../../../Redux/Action/layout'

import {layoutService} from '../../../Services'

class addLayout extends Component {
    constructor(props) {
        super(props)
        this.state = {
            listLayout : [],
        }
        this.onAddLayout = this.onAddLayout.bind(this)
    }
    onAddLayout (_id) {
        const IdForm = this.props.form._id
        this.props.addLayout(IdForm, _id)
        this.props.closePopup()
    }
    componentDidMount() {
        layoutService.readTemplate()
            .then(res => this.setState({ listLayout: res.data.data }))
            .catch(err => console.log(err))
    }
    render() {
        const listLayoutHere = this.state.listLayout
        return (
            <div className='popup_'>
                <div className='popup__inner'>
                    <table>
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Add Layout</th>
                            </tr>
                        </thead>
                        <tbody>
                            {listLayoutHere.map(e => (
                                <tr key={e._id}>
                                    <td>{e.title}</td>
                                    <td>
                                        <button onClick={() => this.onAddLayout(e._id)}> + </button>
                                    </td>
                                </tr>
                            ))}
                        </tbody>
                        <tfoot>
                            <tr>
                                <td>End layout</td>
                                <td>---</td>
                            </tr>
                        </tfoot>
                    </table>
                    <button onClick={this.props.closePopup}>close</button>
                </div>  
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        form: state.form,
    }
}
const mapDispatchToProps = (dispatch, props) => {
    return {
        addLayout : (idForm, _id) => {
            dispatch(LayoutAction.addLayout(idForm, _id))
        }
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(addLayout)