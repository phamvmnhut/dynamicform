import React, { Component } from 'react'

import {connect} from 'react-redux'
import * as LayoutAction from '../../../Redux/Action/layout'

class delLayout extends Component {
    constructor(props) {
        super(props)
        this.onDelLayout = this.onDelLayout.bind(this)
    }
    onDelLayout = () => {
        const IdForm = this.props.form._id
        const _id = this.props.layout._id
        this.props.delLayout(IdForm, _id)
        this.props.closePopup()
    }
    render() {
        return (
            <div className='popup_'>
                <div className='popup__inner'>
                    <span>Do you sure to delete this layout ?</span>
                    <div >
                        <button onClick={this.onDelLayout}>yes</button>
                        <button onClick={this.props.closePopup}>no</button>
                    </div>
                </div>  
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        form: state.form,
        layout: state.layout
    }
}
const mapDispatchToProps = (dispatch, props) => {
    return {
        delLayout : (idForm, _id) => {
            dispatch(LayoutAction.deleteLayout(idForm, _id))
        }
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(delLayout)