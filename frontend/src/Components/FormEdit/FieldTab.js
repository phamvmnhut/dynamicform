import React, { Component } from 'react'

import { fieldService } from '../../Services'

import AddField from './FieldMenu/addField'

const listAdvanceField = [
    {_id: 1, titleDis: "advance Field 1 Here"},
    {_id: 2, titleDis: "advance Field 2 Here"},
    {_id: 3, titleDis: "advance Field 3 Here"}
]

export default class FieldTab extends Component {
    constructor(props) {
        super(props)
        this.state = {
            listField: [],
            selectedField: '',
            showBaseField: true,
            showAdvanceField: false,
            showAddField: false
        }
        this.showBaseField = this.showBaseField.bind(this)
        this.showAdvanceField = this.showAdvanceField.bind(this)
        this.showAddField = this.showAddField.bind(this)
    }
    showBaseField = () => { this.setState({ showBaseField: !this.state.showBaseField }) }
    showAdvanceField = () => { this.setState({ showAdvanceField: !this.state.showAdvanceField }) }
    showAddField = () => { this.setState({ showAddField: !this.state.showAddField }) }

    onChangeLayoutSelect(_id) {this.setState({ selectedField: _id })}

    componentDidMount() {
        fieldService.readTemplate()
            .then(res => this.setState({ listField: res.data.data }))
            .catch(err => console.log(err))
    }
    render() {
        const listBaseFieldHere = this.state.listField
        const listAdvanceFieldHere = listAdvanceField
        const selectedFieldHere = this.state.selectedField
        return (
            <div className="wrap_field_tab">
                <div className="header_field_tab">
                    <span>Menu list Field</span>    
                </div> 
                <div className="group_field">
                    <div className="fa fa-check-square fa-2x" />
                    <span>Base Field</span>
                    {this.state.showBaseField
                        ? <div className="fa fa-chevron-circle-left" onClick={this.showBaseField} />
                        : <div className="fa fa-chevron-circle-down" onClick={this.showBaseField} />
                    }
                </div>
                {this.state.showBaseField
                    ? <div className="list_field">
                        {listBaseFieldHere.map(e => (
                            selectedFieldHere === e._id
                                ? <div key={e._id} >
                                    <span className="selected" onClick={() => this.onChangeLayoutSelect(e._id)} >{e.titleDis}</span>
                                    <AddField _id={e._id} />
                                </ div>
                                : <span key={e._id} onClick={() => this.onChangeLayoutSelect(e._id)} >{e.titleDis}</span>
                        ))}
                    </div>
                    : <div className="list_field_none" />
                }
                <div className="group_field">
                    <div className="fa fa-trophy fa-2x " />
                    <span>Advance Field</span>
                    {this.state.showAdvanceField
                        ? <div className="fa fa-chevron-circle-left" onClick={this.showAdvanceField} />
                        : <div className="fa fa-chevron-circle-down" onClick={this.showAdvanceField} />
                    }
                </div>
                {this.state.showAdvanceField
                    ? <div className="list_field">
                        {listAdvanceFieldHere.map(e => (
                            selectedFieldHere === e._id
                                ? <div key={e._id} >
                                    <span className="selected" onClick={() => this.onChangeLayoutSelect(e._id)} >{e.titleDis}</span>
                                    <AddField _id={e._id} />
                                </ div>
                                : <span key={e._id} onClick={() => this.onChangeLayoutSelect(e._id)} >{e.titleDis}</span>
                        ))}
                    </div>
                    : <div className="list_field_none" />
                }
                <div className="group_field">
                    <div className="fa fa-plus-square fa-2x " />
                    <span>Add your Field</span>
                    {this.state.showAddField
                        ? <div className="fa fa-chevron-circle-left" onClick={this.showAddField} />
                        : <div className="fa fa-chevron-circle-down" onClick={this.showAddField} />
                    }
                </div>
                {this.state.showAddField
                    ? <div className="code_your_field">
                        <textarea />
                        <button>Add my Lib Field</button>
                    </div>
                    : <div className="list_field_none" />
                }
            </div>
        )
    }
}