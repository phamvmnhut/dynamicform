import React, { Component } from 'react'

import {connect} from 'react-redux'
import * as LayoutAction from '../../Redux/Action/layout'
import AddLayout from './LayoutMenu/addLayout'
import EditLayout from './LayoutMenu/editLayout'
import RangeLayout from './LayoutMenu/rangeLayout'
import DelLayout from './LayoutMenu/delLayout'

import LayoutFieldArr from './LayoutMenu/LayoutFieldArr'

import { Menu, Item, Separator, contextMenu } from 'react-contexify';
import 'react-contexify/dist/ReactContexify.min.css';

const Edit = ({ event, props }) => {
    props.handlerMenu(1)
}
const Arange = ({ event, props }) => {
    props.handlerMenu(2)
}
const Del = ({ event, props }) => {
    props.handlerMenu(4)
}

const MyMenu = () => (
    <Menu id='menu_layout'>
        <Item onClick={Edit}>Edit Info</Item>
        <Item onClick={Arange}>Range</Item>
        <Item disabled >Duplicate</Item>
        <Separator />
        <Item onClick={Del}>Delete</Item>
    </Menu>
);

class LayoutMana extends Component {
    constructor(props) {
        super(props)
        this.state = {
            LayoutSelected: 0,
            hideAddLayout: true,
            hideEditLayout: true,
            hideRangeLayout: true,
            hideDelLayout: true
        }
        this.handlerMenu = this.handlerMenu.bind(this)
        this.togglePopupAddLayout = this.togglePopupAddLayout.bind(this)
        this.togglePopupEditLayout = this.togglePopupEditLayout.bind(this)
        this.togglePopupRangeLayout = this.togglePopupRangeLayout.bind(this)
        this.togglePopupDelLayout = this.togglePopupDelLayout.bind(this)
    }
    handlerMenu = (index, number = null) => { 
        switch (index) {
            case 0:
                this.handleGetLayout(number)
                this.setState({ LayoutSelected: number })
                break;
            case 1:
                this.setState({hideEditLayout: false})
                break;
            case 2:
                this.setState({hideRangeLayout: false})
                break;
            case 3:
                this.setState({hideRangeLayout: false})
                break;
            case 4:
                this.setState({hideDelLayout: false})
                break;
            default:
                break;
        }
    }
    onContextMenu = (e, func) => {
        e.preventDefault();
        contextMenu.show({
            id: 'menu_layout',
            event: e,
            props: {
                "handlerMenu": func
            }
          });
    }
    togglePopupAddLayout = () => {this.setState({hideAddLayout: !this.state.hideAddLayout});}
    togglePopupEditLayout = () => {this.setState({hideEditLayout: !this.state.hideEditLayout});}
    togglePopupRangeLayout = () => {this.setState({hideRangeLayout: !this.state.hideRangeLayout})}
    togglePopupDelLayout = () => {this.setState({hideDelLayout: !this.state.hideDelLayout})}
    
    handleGetLayout(SelectedHere){
        const {form} = this.props
        const _id = this.props.form.FormLayout[SelectedHere] 
        this.props.sendLayoutCur(form._id, _id)
    }
    componentDidUpdate(preProp){
        if (preProp.form !== this.props.form){this.handleGetLayout(0)}
    }
    render() {
        const {form} = this.props
        if (!form._id) {
            return (
                <div className="wrap_layout_mana">
                    You dont select any FORM
                </div>
            )
        }
        const SelectedHere = this.state.LayoutSelected
        return (
            <div className="wrap_layout_mana">
                <div className="header_layout_mana">
                    <span>Menu list layout</span>
                </div>
                <div className="list_layout scroller">
                    {form.FormLayout.map((e, i) => (
                        SelectedHere === i
                            ? <div key={e}>
                            <button id="menu_layout"  className="selected" value={i}
                                onClick={() => this.handlerMenu(0, i)}
                                onContextMenu={(e) => this.onContextMenu(e, this.handlerMenu)}>
                                {e}
                            </button>
                            <LayoutFieldArr />
                            </div>
                            : <button id="menu_layout" key={e} value={i}
                                onClick={() => this.handlerMenu(0, i)}>
                                {e}
                            </button>
                    ))}
                    <MyMenu />
                    <button onClick={this.togglePopupAddLayout} >
                        +
                    </button>
                    {
                        !this.state.hideAddLayout &&
                        <AddLayout closePopup={this.togglePopupAddLayout} />
                    }
                    {
                        !this.state.hideEditLayout &&
                        <EditLayout closePopup={this.togglePopupEditLayout} />
                    }
                    {
                        !this.state.hideRangeLayout &&
                        <RangeLayout closePopup={this.togglePopupRangeLayout} />
                    }
                    {
                        !this.state.hideDelLayout &&
                        <DelLayout closePopup={this.togglePopupDelLayout} />
                    }
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        form : state.form
    }
}
const mapDispatchToProps = (dispatch, props) => {
    return {
        sendLayoutCur : (idForm, _id) => {
            dispatch(LayoutAction.fetchLayout(idForm, _id))
        }
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(LayoutMana)