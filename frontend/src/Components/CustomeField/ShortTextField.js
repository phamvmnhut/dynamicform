import React, { Component } from 'react';
import { connectField } from 'uniforms';

class ShortTextField extends Component {
  componentDidMount() {
    this.props.onChange("")
  }
  render() {
    const {titleDis ,onChange} = this.props
    return (
      <div className="wrap_field">
        <label>{titleDis}</label>
        <input onChange={(e) => onChange(e.target.value)}></input>
    </div>
    )
  }
}

export default connectField(ShortTextField);