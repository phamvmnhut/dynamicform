import React from 'react';
import { AutoField } from 'uniforms-semantic';
import { connectField } from 'uniforms';

const Range = ({ value: { start, stop } }) => (
    <section>
      <AutoField InputLabelProps={{ shrink: true }} name="start" max={stop} />
      <AutoField InputLabelProps={{ shrink: true }} name="stop" min={start} />
    </section>
  );
  
  const RangeField = connectField(Range);