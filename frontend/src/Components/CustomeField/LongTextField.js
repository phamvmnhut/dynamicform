import React, { Component } from 'react';
import { connectField } from 'uniforms';

class LongTextField extends Component {
  componentDidMount() {
    this.props.onChange("")
  }
  render() {
    const {titleDis ,onChange} = this.props
    return (
      <div className="wrap_field">
        <label>{titleDis}</label>
        <textarea onChange={(e) => onChange(e.target.value)}></textarea>
    </div>
    )
  }
}

export default connectField(LongTextField);