import MyCusField from './MyCusField'
import RatingField from './RatingField'
import ImageField from './ImageField'
import ShortTextField from './ShortTextField'
import TitleField from './TitleField'
import LongTextField from './LongTextField'
import OptionField from './OptionField'

export default (name) => {
    switch (name) {
        case 'RatingField':
            return RatingField
        case 'ImageField':
            return ImageField
        case 'LongTextField':
            return LongTextField
        case 'ShortTextField':
            return ShortTextField
        case 'TitleField':
            return TitleField
        case 'OptionField':
            return OptionField
        default:
            return MyCusField
    }
}

