import React from 'react';
import { connectField } from 'uniforms';

function TitleField({titleDis, description}) {
  return (
    <div className="wrap_field">
        <h3>{titleDis}</h3>
        <p>{description}</p>
    </div>
  );
}

export default connectField(TitleField);