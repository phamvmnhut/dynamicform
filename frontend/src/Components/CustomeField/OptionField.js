import React, { Component } from 'react'
import { connectField } from 'uniforms';

class OptionField extends Component {
    componentDidMount() {
        this.props.onChange(this.props.option[0])
    }
    render() {
        const {titleDis, option = [], onChange} = this.props
        return (
            <div className="wrap_field">
            <label>{titleDis}</label>
            <select onChange={(e) => onChange(e.target.value)}>
                {option.map((e,i) =>(
                    <option key={i}>{e}</option>
                ))}
            </select>
        </div>
        )
    }
}

export default connectField(OptionField);