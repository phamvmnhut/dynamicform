import React, { Component } from 'react'
import classnames from 'classnames';

import { connectField } from 'uniforms';

class RatingField extends Component {
  componentDidMount() {
    this.props.onChange(0)
  }
  render() {
    const {titleDis, className, disabled, max = 5, required, value, onChange} = this.props
    return (
      <div className="wrap_field">
      <label>{titleDis}</label>
      <section className={classnames('ui', { disabled, required }, className)}>
        {Array.from({ length: max }, (_, index) => index + 1).map(index => (
          <span
            style={{ fontSize: 40, cursor: 'pointer' }}
            key={index}
            onClick={() =>
              disabled || onChange(!required && value === index ? null : index)
            }
          >
            {index <= value ? '★' : '☆'}
          </span>
        ))}
      </section>
  </div>
    )
  }
}

export default connectField(RatingField);