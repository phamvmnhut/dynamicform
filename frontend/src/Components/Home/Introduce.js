import React, { Component } from 'react'

import {Link} from 'react-router-dom'

export default class Introduce extends Component {
    render() {
        return (
            <div className="wrap_secssion intro">
                <div className="intro__left">
                    <div className="background_left">
                        <h1>DYNAMIC FORM</h1>
                        <span>Dynamic is you dont must think</span>
                    </div>
                </div>
                <div className="intro__right">
                    <Link to={'/user'} >
                        <button >USING NOW</button>
                    </Link>
                </div>

            </div>
        )
    }
}
