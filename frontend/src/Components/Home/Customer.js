import React, { Component } from 'react'

export default class Customer extends Component {
    render() {
        return (
            <div className="wrap_secssion customer">
                <h2>Khách hàng sử dụng</h2>
                <h5>Bạn đang có nhu cầu tạo ra các khảo sát, tìm hiểu ý suy nghĩ người tiêu dùng, đánh giá mức độ hài lòng, hay muốn khảo sát về một việc</h5>
                <h3> Hãy sử dụng Dynamic Form chúng tôi </h3>
                <span>Chúng tôi cung cấp dịch vụ giúp bạn giải quyết những vấn đề trên chỉ cần bạn làm vài click chuột</span>
            </div>
        )
    }
}
