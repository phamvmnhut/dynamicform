import React, { Component } from 'react'

import {Link} from 'react-router-dom'

export default class UsingNow extends Component {
    render() {
        return (
            <div className="wrap_secssion using_now">
                <h1>Chỉ 5 phút thao tác có ngay Form đẹp</h1>
                <Link to={'/user'} >
                    <button >GETTING STARTED</button>
                </Link>
            </div>
        )
    }
}
