import React, { Component } from 'react'

export default class Feature extends Component {
    render() {
        return (
            <div className="wrap_secssion feature">
                <div className="wrap_feature">
                    <div className="feature_one">
                        <div className="fa fa-forward fa-5x"></div>
                        <h3>Nhanh chóng tạo ra sản phẩm với các mấu có sẳn</h3>
                    </div>
                    <div className="feature_one">
                        <div className="fa fa-link fa-5x"></div>
                        <h3>Dàng chia sẻ mọi người với đường link trực tiếp</h3>
                    </div>
                    <div className="feature_one">
                    <div className="fa fa-info-circle fa-5x"></div>
                        <h3>Quản lý kết quả sử dụng mẫu một cách tiện lợi</h3>
                    </div>
                </div>
            </div>
        )
    }
}
