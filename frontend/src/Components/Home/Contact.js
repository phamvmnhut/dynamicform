import React, { Component } from 'react'

export default class Contact extends Component {
    render() {
        return (
            <div className="wrap_secssion contact">
                <i className="fab fa-connectdevelop fa-5x"></i>
                <p>Phạm Văn Minh Nhựt - PVMN</p>
                <p>Sourecode: <a target="_blank" href="https://gitlab.com/phamvmnhut/dynamicform.git">https://gitlab.com/phamvmnhut/dynamicform.git</a></p>
                <p>Địa chỉ: ktx khu B - Đại học quốc gia TP HCM</p>
                <p>Hotline:  0338473427</p>
                <p>Email: phamvmnhut@gmail.com</p>
            </div>
        )
    }
}
