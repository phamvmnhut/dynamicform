import React, { Component } from 'react'
import { Redirect } from 'react-router'

import {formService} from '../../Services'

import { Menu, Item, Separator, contextMenu } from 'react-contexify';
import 'react-contexify/dist/ReactContexify.min.css';

import DelForm from './FormMenu/delForm';
import ShowLayout from './FormMenu/showLayout'
import DataForm from './FormMenu/viewData'
import EditForm from './FormMenu/editForm'

const Show = ({ event, props }) => { props.handlerMenu(1, event.target.getAttribute('value')) }
const Edit = ({ event, props }) => { props.handlerMenu(2, event.target.getAttribute('value')) }
const GoEdit = ({ event, props }) => { props.handlerMenu(3, event.target.getAttribute('value')) }
const View = ({ event, props }) => { props.handlerMenu(4, event.target.getAttribute('value')) }
const Data = ({ event, props }) => { props.handlerMenu(5, event.target.getAttribute('value')) }
const Del = ({ event, props }) => { props.handlerMenu(6, event.target.getAttribute('value')) }

const MyMenu = () => (
    <Menu id='menu_form'>
        <Item disabled onClick={Show}>Show Layout</Item>
        <Item onClick={Edit}>Edit Title</Item>
        <Item onClick={GoEdit}>DashBoard Edit Form</Item>
        <Separator />
        <Item onClick={View}>Using Form</Item>
        <Item onClick={Data}>View Data</Item>
        <Separator />
        <Item onClick={Del}>Delete</Item>
    </Menu>
);

class HaveForm extends Component {
    constructor(props) {
        super(props)
        this.state = {
            "formCurID" : "",
            "showDelForm" : false,
            "viewForm" : false,
            "showDataForm" : false,
            "showEditForm" : false,
            "goEditForm" : false,
            "showLayout" : false
        }
        this.handlerMenu = this.handlerMenu.bind(this)
        this.closePopupDelForm = this.closePopupDelForm.bind(this)
        this.closePopupEditForm = this.closePopupEditForm.bind(this)
        this.closePopupDataForm = this.closePopupDataForm.bind(this)
        this.delForm = this.delForm.bind(this)
        this.editForm = this.editForm.bind(this)
    }

    handlerMenu = (index, ID = null) => { 
        switch (index) {
            case 1:
                this.setState({"formCurID": ID, "showLayout": true})
                break;
            case 2:
                this.setState({"formCurID": ID, "showEditForm": true})
                break;
            case 3:
                this.setState({"formCurID": ID, "goEditForm": true})
                break;
            case 4:
                this.setState({"formCurID": ID, "viewForm": true})
                break;
            case 5:
                this.setState({"formCurID": ID, "showDataForm": true})
                break;
            case 6: 
                this.setState({"formCurID": ID, "showDelForm": true})
                break;
            default:
                break;
        }
    }
    closePopupDelForm = () => {this.setState({"showDelForm": false})}
    closePopupEditForm = () => {this.setState({"showEditForm": false})}
    closePopupDataForm = () => {this.setState({"showDataForm": false})}

    delForm = () => {
        const _id = this.state.formCurID
        formService.deleteOne(_id)
            .then(res => {
                if (res.data.status === 'success') {
                    const Arr = this.props.ArrForm
                    let i = 0
                    for (i = 0; i < Arr.length; i++) {
                        if (Arr[i]._id === _id) break;
                    }
                    Arr.splice(i, 1)
                    this.props.changeMyForm(Arr)
                }
            })
    }
    editForm = (data) => {
        const _id = this.state.formCurID
        formService.updateOne(_id, data)
            .then(res => {
                if (res.data.status === 'success') {
                    const Arr = this.props.ArrForm
                    let i = 0
                    for (i = 0; i < Arr.length; i++) {
                        if (Arr[i]._id === _id) break;
                    }
                    Arr[i] = res.data.data
                    this.props.changeMyForm(Arr)
                }
            })
    }

    onContextMenu = (e, func) => {
        e.preventDefault();
        contextMenu.show({
            id: 'menu_form',
            event: e,
            props: { "handlerMenu": func }
        });
    }
    render() {
        if (this.state.goEditForm) {
            let linkEdit = `form/${this.state.formCurID}`;
            return <Redirect to={linkEdit} push={true} />
        }
        if (this.state.viewForm) {
            let linkEdit = `link/${this.state.formCurID}`;
            return <Redirect to={linkEdit} push={true} />
        }
        const Arr = this.props.ArrForm
        let i = 0
        const _id = this.state.formCurID
        for (i = 0; i < Arr.length; i++) {
            if (Arr[i]._id === _id) break;
        }
        return (
            <div className="wrap_have">
                {Arr.map((e,i) => (
                    <div className="itemForm" key={i} onClick={(evt)=>this.onContextMenu(evt, this.handlerMenu)}>
                        <div className="form_left">
                            <div className="fa fa-star icon_star fa-2x" ></div>
                            <div className="text_info" >
                                <h2 value={e._id} >{e.title}</h2>
                                <h5>{e.description}</h5>
                            </div>
                        </div>
                        <div className="form_right">
                            <span>{e.FormLayout.length} page </span>
                        </div>
                    </div>
                ))}
                <MyMenu />
                <div className="itemForm" >
                    <div className="form_left">
                    <div className="fa fa-star icon_star fa-2x" ></div>
                    <div className="text_info" >
                        <h2 >Title Form</h2>
                        <h5>description</h5>
                    </div>
                    </div>
                    <div className="form_right">
                        <span>1 page </span>
                    </div>
                </div>
                <div className="itemLayout" >
                        <div className="fa fa-backward icon_layout"></div>
                        <div className="text_info" >
                        <h3 >Title layout</h3>
                        <h5>description</h5>
                        </div>
                </div>
                {
                    this.state.showDelForm && 
                    <DelForm
                        onClose={this.closePopupDelForm}
                        onDelForm={this.delForm}/>
                }
                {
                    this.state.showEditForm && 
                    <EditForm
                        data={Arr[i]}
                        onClose={this.closePopupEditForm}
                        onEditForm={this.editForm}/>
                }
                {
                    this.state.showDataForm &&
                    <DataForm
                        data={Arr[i]}
                        onClose={this.closePopupDataForm} />
                }
                {
                    this.state.showLayout &&
                    <ShowLayout _id=""/>
                }
                
            </div>
        )
    }
}

class NotHaveForm extends Component {
    render() {
        return (
            <div className="wrap_not_have">
                <div className="img_dont">
                    <span>Illustrator icon for</span>
                    <span>DONT HAVE ANY FORM YET</span>
                </div>
                <button onClick={() => this.props.changeMyform(2)}>
                    Create New Form
                </button>
            </div>
        )
    }
}

export default class AllForm extends Component {
    constructor(props) {
        super(props)
        this.state = {
            ArrForm : []
        }
        this.changeMyForm = this.changeMyForm.bind(this)
    }
    componentDidMount() {
        formService.fetch()
        .then(res => {
            this.setState({ArrForm: res.data.data})
        })
        .catch(err => console.log(err))
    }
    changeMyForm = (Arr) => {this.setState({"ArrForm": Arr})}
    
    render() {
        return (
            <div className="wrap_formmana">
                {this.state.ArrForm.length === 0 
                ? <NotHaveForm changeMyform={this.props.changeMyform}/>
                : <HaveForm ArrForm={this.state.ArrForm} SendToEdit={this.props.SendToEdit} changeMyForm={this.changeMyForm}/>}
            </div>
        )
    }
}
