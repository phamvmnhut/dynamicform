import React, { Component } from 'react'

import {dataService} from '../../../Services'

export default class editLayout extends Component {
    constructor(props) {
        super(props)
        this.state = {
            "arrTitle" : [""],
            "arrData" : [
                [""],
                [""]
            ]
        }   

        this.deleteRecord = this.deleteRecord.bind(this)
    }

    deleteRecord = (i) => {
        const _id = this.state.arrData[i][0]
        dataService.deleteOne(_id)
            .then(res => {
                const newArrData = [...this.state.arrData]
                newArrData.splice(i,1)
                this.setState({"arrData": newArrData})
                alert('delete success')
            })
            .catch(err => {
                console.log(err)
            })
    }

    componentDidMount() {
        const {data} = this.props
        if (data === undefined) return 
        dataService.readOne(data._id)
            .then(res => {
                const temp = res.data.data
                let arrTitle = ["_id"]
                for (let i = 0; i <temp.length; i++) {
                    const CurRow = temp[i]
                    for (let x in CurRow) {
                        let index
                        for (index = 0; index <arrTitle.length; index++) if (arrTitle[index] === x) break
                        if (index === arrTitle.length) arrTitle.push(x)
                    }
                }
                let arrData = []
                const arrDataSample = []
                for (let i = 0; i< arrTitle.length;i++) {
                    arrDataSample.push(" ")
                }
                for (let i = 0; i < temp.length; i++) {
                    let arrDataHere = [...arrDataSample]
                    let CurRow = temp[i]
                    
                    for (let x in CurRow) {
                        let index
                        for (index = 0; index <arrTitle.length; index++) {
                            if (arrTitle[index] === x) break;
                        }
                        arrDataHere.splice(index, 1, CurRow[x])
                    }
                    
                    console.log('[Here]', arrDataHere)
                    arrData.push(arrDataHere)
                }
                console.log(arrData)
                this.setState({
                    "arrTitle": arrTitle,
                    "arrData": arrData
                })
            })
            .catch(err => console.log(err))
    }
    
    render() {
        const {arrTitle, arrData} = this.state
        return (
            <div className='popup_'>
                <div className='popup__inner'>
                    <table>
                        <thead>
                            <tr>
                                <th>STT</th>
                                {arrTitle.map((e,i)=> (
                                    <th key={i}>{e}</th>
                                ))}
                                <th>Delete</th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                arrData.map((row, i) => {
                                    return (
                                        <tr key={i}>
                                            <td>{i}</td>
                                            {
                                                row.map((e,id)=>(
                                                    <td key={id}>{e}</td>
                                                ))
                                            }
                                            <td><button onClick={()=> this.deleteRecord(i)}>X</button></td>
                                        </tr>
                                    )
                                })
                            }
                        </tbody>
                    </table>
                    <div >
                        <button onClick={this.props.onClose}>close</button>
                    </div>
                    
                </div>  
            </div>
        )
    }
}