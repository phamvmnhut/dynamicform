import React, { Component } from 'react'

export default class delField extends Component {
    async delForm () {
        await this.props.onDelForm()
        this.props.onClose()
    }
    render() {
        return (
            <div className='popup_'>
                <div className='popup__inner'>
                    <span>Do you sure to delete this form ?</span>
                    <div >
                        <button onClick={() =>this.delForm()}>sure</button>
                        <button onClick={this.props.onClose}>no</button>
                    </div>
                </div>  
            </div>
        )
    }
}