import React, { Component } from 'react'

export default class editLayout extends Component {
    constructor(props) {
        super(props)
        this.state = {
            title : "",
            description: ""
        }   
    }
    onEditForm = async () => {
        const data = {
            "title": this.state.title,
            "description": this.state.description
        }
        await this.props.onEditForm(data)

        this.props.onClose()
    }
    componentDidMount() {
        const {data} = this.props
        if (data.title === undefined) return 
        this.setState({
            title : data.title,
            description: data.description
        })
    }
    
    render() {
        return (
            <div className='popup_'>
                <div className='popup__inner'>
                    <table>
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Edit</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Title</td>
                                <td>
                                    <input
                                        value={this.state.title}
                                        onChange={(e) => this.setState({ title: e.target.value })}>
                                    </input>
                                </td>
                            </tr>
                            <tr>
                                <td>Description</td>
                                <td>
                                    <textarea
                                        value={this.state.description}
                                        onChange={(e) => this.setState({ description: e.target.value })}>
                                    </textarea>
                                </td>
                            </tr>
                        </tbody>
                        <tfoot>
                            <tr>
                                <td>End Field</td>
                                <td>---</td>
                            </tr>
                        </tfoot>
                    </table>
                    <div >
                        <button onClick={() =>this.onEditForm()}>save</button>
                        <button onClick={this.props.onClose}>close</button>
                    </div>
                    
                </div>  
            </div>
        )
    }
}