import React, { Component } from 'react'

export default class navbar extends Component {
    constructor(props){
        super(props)
        this.state ={
            choice: 0
        }
    }
    selectChoice = (num) =>{
        this.setState({choice: num})
        this.props.changeMyform(num)
    }
    render() {
        const myform = this.props.myform;
        return (
            <div className="navbar_form">
                <div className="myform_section">
                    <h1 className="section_title">MY FORM</h1>
                    <button 
                    className={myform === 1 ? "section_btn_choice" : ""} 
                    onClick={ () => this.selectChoice(1)}>
                        All Form
                    </button>
                    <button
                        className={myform === 2 ? "section_btn_choice" : ""}
                        onClick={() => this.selectChoice(2)}>
                        Create New Form
                    </button>
                </div>
            </div>
        )
    }
}
