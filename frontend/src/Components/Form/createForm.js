import React, { Component } from 'react'
import { Redirect } from 'react-router'

import {formService} from '../../Services'

export default class createForm extends Component {
    constructor(props) {
        super(props);
        this.state= {
            isLoading : true,
            itemForm : 0,
            redirectEdit : "",
            ArrFormTemplate : []
        }
        this.createForm = this.createForm.bind(this);
    }
    changeItemForm = (num) =>{this.setState( {"itemForm": num})}

    createForm = async () => {
        const data = { idForm : this.state.ArrFormTemplate[this.state.itemForm]._id} 
        const doc = await formService.createById(data)
        this.setState({redirectEdit: doc.data.data._id})
        
    }
    componentDidMount() {
        // fetch Form template
         formService.readTemplate()
         .then(res => {
             this.setState({
                 ArrFormTemplate: res.data.data,
                 isLoading : false
             })
         })
         .catch(err=> console.log(err))
    }
    render() {
        if (this.state.redirectEdit !== "") {
            let linkEdit = `form/${this.state.redirectEdit}`;
            return <Redirect to={linkEdit} push={true} />
          }
        if (this.state.isLoading || this.state.ArrFormTemplate.length === 0) {
            return (
                <div className="wrap_create_form">
                    <span>Loading Form template ...</span>
                </div>
            )
        }
        const FormTemplateHere = this.state.ArrFormTemplate[this.state.itemForm]
        return (
            <div className="wrap_create_form">
                <div className="list_form">
                    <table>
                        <thead>
                            <tr>
                                <th>STT</th>
                                <th> Name</th>
                                <th> Description</th>
                                <th> Number</th>
                            </tr>
                        </thead>
                        <tbody>
                            {this.state.ArrFormTemplate.map((e, i) => {
                                const trSelect = (i === this.state.itemForm) ? "selected" : ""
                                return (
                                    <tr key={i} className={trSelect} onClick={() => this.changeItemForm(i)}>
                                        <td>{i}</td>
                                        <td> {e.title}</td>
                                        <td> {e.description}</td>
                                        <td>{e.FormLayout.length}</td>
                                    </tr>)
                            })}
                        </tbody>
                    </table>
                    
                    <div className="use_form">
                        <div className="form_avt">
                            <h4>{FormTemplateHere.title}</h4>
                            <span>{FormTemplateHere.description}</span>
                        </div>
                        <button onClick={this.createForm}>USE FORM</button>
                    </div>
                </div>
            </div>
        )
    }
};

