import React, { Component } from 'react'
import { Redirect } from 'react-router'

export default class Login extends Component {
    constructor(props) {
        super(props)
        this.state = {
            username : "",
            pwd: "",
            isSuccess: false
        }
        this.onLogin = this.onLogin.bind(this)
    }
    onLogin() {
        this.setState({ isSuccess: true })
    }
    render() {
        if (this.state.isSuccess) {
            return <Redirect to={"/form"} push={true} />
        }
        return (
            <div className='popup_'>
                <div className='popup__inner'>
                    <span>Login User Form</span>
                    <div className="form_user">
                        <span>UserName</span>
                        <input
                            value={this.state.username}
                            onChange={(e) => this.setState({ username: e.target.value })} />
                        <span>PassWord</span>
                        <input
                        type="password"
                            value={this.state.pwd}
                            onChange={(e) => this.setState({ pwd: e.target.value })} />
                        <button onClick={this.onLogin}>Login</button>
                        <button onClick={this.props.onClose}>Cancel</button>
                    </div>
                </div>
            </div>
        )
    }
}