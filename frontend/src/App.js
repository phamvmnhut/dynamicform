import React from 'react';
import './Styles/main.scss'
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";

import Home from './Screens/Home'
import User from './Screens/User'
import Form from './Screens/Form'
import FormEdit from './Screens/FormEdit'
import Link from './Screens/Link'
import PageNotFound from './Screens/PageNotFound'

function App() {
  return (
    <Router>
        <Switch>
          <Route path="/" exact component={Home} />
          <Route path="/user" component={User} />
          <Route path="/form" exact component={Form} />
          <Route path="/form/:idForm" exact component={FormEdit} />
          <Route path="/link/:idForm" exact component={Link} />
          <Route path="/" component={PageNotFound} />
        </Switch>
    </Router>
  );
}

export default App;
