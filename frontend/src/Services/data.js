import axios from 'axios'

const HOST = require('./host').host;

class DataService{
    readOne(idForm) {
        return axios({
            method: "GET",
            url: `${HOST}/data?idform=${idForm}`,
        })
    }

    submit(data) {
        return axios({
            method: "POST",
            url: `${HOST}/data/`,
            data: data
        })
    }
    
    deleteOne(idData) {
        return axios({
            method: "DELETE",
            url: `${HOST}/data/${idData}`
        })
    }
}
export default DataService