import FormService from "./form";
import UserService from "./user";
import LayoutService from './layout'
import FieldService from './field'
import DataService from './data'

export const formService = new FormService()
export const userService = new UserService()
export const layoutService = new LayoutService()
export const fieldService = new FieldService()
export const dataService = new DataService()