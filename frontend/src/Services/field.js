import axios from 'axios'

const HOST = require('./host').host;

class FieldService{

    // read Field Template
    readTemplate() {
        return axios({
            method: "GET",
            url: `${HOST}/template/field`,
        })
    }
    // create New Field
    createById(idForm, idLayout, data){
        return axios({
            method:"POST",
            url: `${HOST}/form/${idForm}/layout/${idLayout}/field/`,
            data : data
        })
    }

    // read One Field
    readOne(idForm, idLayout, idField) {
        return axios({
            method: "GET",
            url: `${HOST}/form/${idForm}/layout/${idLayout}/field/${idField}`,
        })
    }
    
    // update One Field
    updateOne(idForm, idLayout, idField, data) {
        return axios({
            method: "PATCH",
            url: `${HOST}/form/${idForm}/layout/${idLayout}/field/${idField}`,
            data: data
        })
    }
    // delete One Field
    deleteOne(idForm, idLayout, idField) {
        console.log('test')
        return axios({
            method: "DELETE",
            url: `${HOST}/form/${idForm}/layout/${idLayout}/field/${idField}`,
        })
    }
}
export default FieldService