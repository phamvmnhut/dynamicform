import Ajv from 'ajv';
import { JSONSchemaBridge } from 'uniforms-bridge-json-schema';

import ChangeToField from '../Components/CustomeField'

// define validate for form
const ajv = new Ajv({ allErrors: true, useDefaults: true });
function createValidator(schema) {
    const validator = ajv.compile(schema);

  return model => {
    validator(model);

    if (validator.errors && validator.errors.length) {
      // eslint-disable-next-line no-throw-literal
      throw { details: validator.errors };
    }
  };
}

export const LayoutToForm = (data) => {
  const FieldArr = data.FormField

  let propSchema = {}

  for (let i = 0; i < FieldArr.length; i++) {
    const FieldItem = FieldArr[i]

    const FieldItemStr = JSON.stringify(FieldItem)
    const FieldArrStr2 = FieldItemStr.substring(1, FieldItemStr.length - 1)
    let newFieldStr = `{ ${FieldArrStr2}, "uniforms": {"component": "${FieldItem.component}" } }`
    let newField = JSON.parse(newFieldStr)
    newField.uniforms.component = ChangeToField(FieldItem.component)
    propSchema[FieldItem._id] = newField
  }
  const schema = {
    title: data.title,
    description: data.description,
    type: 'object',
    properties: { ...propSchema }
  };
  const schemaValidator = createValidator(schema);
  // correct form 
  return new JSONSchemaBridge(schema, schemaValidator);
}
