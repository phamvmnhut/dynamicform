import axios from 'axios'

const HOST = require('./host').host;

class LayoutService{

    // read Layout Template
    readTemplate() {
        return axios({
            method: "GET",
            url: `${HOST}/template/layout`,
        })
    }
    // create New Layout
    createById(idForm, data){
        return axios({
            method:"POST",
            url: `${HOST}/form/${idForm}/layout`,
            data : data
        })
    }

    // read One Layout
    readOne(idForm, idLayout) {
        return axios({
            method: "GET",
            url: `${HOST}/form/${idForm}/layout/${idLayout}`,
        })
    }
    
    readFull(idForm, idLayout) {
        return axios({
            method: "GET",
            url: `${HOST}/form/${idForm}/layout/${idLayout}/full`,
        })
    }
    
    // update One Layout
    updateOne(idForm, idLayout, data) {
        return axios({
            method: "PATCH",
            url: `${HOST}/form/${idForm}/layout/${idLayout}`,
            data: data
        })
    }
    // delete One Layout
    deleteOne(idForm, idLayout, data) {
        return axios({
            method: "DELETE",
            url: `${HOST}/form/${idForm}/layout/${idLayout}`,
            data: data
        })
    }

}
export default LayoutService