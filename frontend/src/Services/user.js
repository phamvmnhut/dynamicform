import axios from 'axios'
// validate data input

const HOSTGCALLS = require('./host').hostGCall;

class UserService {
    signUp(data) {
        return axios({
            method: 'POST',
            url: `${HOSTGCALLS}/`,
            data: data 
        })
    }
    logIn(data){
        return axios({
            method:'POST',
            url: `${HOSTGCALLS}/`,
            data: data
        })
    }
    fetch(){
        return axios({
            method:"GET",
            url: `${HOSTGCALLS}/`,
        })
    }
    delete(taikhoan){
        const userLogin=JSON.parse(localStorage.getItem("userLogin"))
        return axios({
            method:"DELETE",
            url: `${HOSTGCALLS}/`,
            headers:{
                "x-access-token": userLogin
            }
        })
    }
}
export default UserService
