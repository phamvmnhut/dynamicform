import axios from 'axios'

const HOST = require('./host').host;

class FormService{

    // read Form Template
    readTemplate() {
        return axios({
            method: "GET",
            url: `${HOST}/template/form`,
        })
    }

    // get list user's form 
    fetch(){
        return axios({
            method:"GET",
            url: `${HOST}/form`,
        })
    }
    // create New form
    createById(data){
        return axios({
            method:"POST",
            url: `${HOST}/form`,
            data : data
        })
    }

    // read One Form
    readOne(idForm) {
        return axios({
            method: "GET",
            url: `${HOST}/form/${idForm}`,
        })
    }
    
    // read One Form
    updateOne(idForm, data) {
        return axios({
            method: "PATCH",
            url: `${HOST}/form/${idForm}`,
            data: data
        })
    }
    // delete One Form
    deleteOne(idForm) {
        return axios({
            method: "DELETE",
            url: `${HOST}/form/${idForm}`,
        })
    }

    fecthFormLink(idForm) {
        return axios({
            method: "GET",
            url: `${HOST}/link/${idForm}`
        })
    }

}
export default FormService