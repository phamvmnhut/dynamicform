import React, { Component } from 'react'
import { Redirect } from 'react-router'

export default class PageNotFound extends Component {
    constructor(props){
        super(props)
        this.state = {
            delaytimeout : false
        }
    }
    componentDidMount() {
        this.timer = setTimeout(() => this.setState({delaytimeout: true}), 2000)
      }
      
      componentWillUnmount() {
        clearTimeout(this.timer);
      }
    render() {
        if (this.state.delaytimeout) {
            let linkEdit = "/";
            return <Redirect to={linkEdit} push={true} />
          }
        return (
            <div id="notfound">
                <div className="notfound">
                    <div className="notfound-404">
                        <h1>4<span>0</span>4</h1>
                    </div>
                    <p>The page you are looking for might not have this website </p>
                    <span >Home page</span>
                </div>
            </div>
        )
    }
}

