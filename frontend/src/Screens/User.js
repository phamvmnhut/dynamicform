import React, { Component } from 'react'

import {Link} from 'react-router-dom'

import Login from '../Components/User/Login'
import Register from '../Components/User/Register'

export default class User extends Component {
    constructor(props){
        super(props)
        this.state = {
            showLogin : false,
            showReg : false
        }
        this.togglePopupLogin = this.togglePopupLogin.bind(this)
    }
    
    togglePopupLogin = () => {this.setState({"showLogin" : !this.state.showLogin})}
    togglePopupReg = () => {this.setState({"showReg" : !this.state.showReg})}
    render() {
        return (
            <div className="wrap_user">
                <div className="info_intro">
                    <h1>Mời bạn đăng nhập hoặc đăng kí mới tài khoản để sử dụng úng dụng</h1>
                    <p>Tài khoản sẽ được sử dụng để tạo lập form mới và quản lý điền form của khách hàng, tiện lợi hơn cách sử dụng thông thường</p>
                </div>
                <div className="login-reg">
                    <button onClick={this.togglePopupLogin}>LOGIN</button>
                    <button onClick={this.togglePopupReg}>REGISTER</button>
                </div>
                {
                    this.state.showLogin &&
                    <Login onClose={this.togglePopupLogin}/>
                }
                {
                    this.state.showReg && 
                    <Register onClose={this.togglePopupReg} />
                }
                <Link to="/">
                    <button>Back to Home</button>
                </Link>
            </div>
        )
    }
}
