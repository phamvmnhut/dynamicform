import React, { Component } from 'react'

import Header from '../Components/Home/header'

import Navbar from '../Components/Form/navbar'
import FormMana from '../Components/Form/FormMana'
import CreateForm from '../Components/Form/createForm'

export default class Form extends Component {
    constructor(props) {
        super(props)
        this.state = {
            myform : 1,
        }
        this.changeMyform = this.changeMyform.bind(this);
    }
    changeMyform = (number) => {this.setState( {myform : number})}
    
    render() {
        return (
            <div className="wrap_form">
                <div className="wrap_header">
                    <Header />
                </div>
                <div className="wrap_form_inner">
                    <Navbar changeMyform={this.changeMyform} myform={this.state.myform} />
                    <div className="body_wraper">
                        {this.state.myform === 1
                            ? <FormMana changeMyform={this.changeMyform} />
                            : <CreateForm />}
                    </div>
                </div>
            </div>
        )
    }
}