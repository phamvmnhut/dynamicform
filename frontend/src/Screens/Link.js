import React, { Component } from 'react'

import {connect} from 'react-redux'

import {formService} from '../Services'
import { createAction } from "../Redux/Action"
import * as TYPE from "../Redux/Action/type"

import ViewForm from '../Components/FormEdit/ViewForm'

class Link extends Component {
    constructor(props){
        super(props)
        this.state ={
            isLoading : true,
            layoutCur : 0,
            submitForm: false,
            err : ""
        }
        this.previous = this.previous.bind(this)
        this.next = this.next.bind(this)
    }
    componentDidMount() {
        const id = this.props.match.params.idForm
        formService.fecthFormLink(id)
            .then(res => {
                this.setState({ isLoading: false })
                this.data = res.data.data
                this.props.onLayout(this.data.FormLayout[this.state.layoutCur])
            })
            .catch(err => {
                console.log(err)
                this.setState({ "err": err })
            }
            )
    }
    previous () {
        if (this.state.layoutCur < 1) {
            alert("This is first of form")
        } else {
            this.setState({ "layoutCur": this.state.layoutCur - 1 }, () => {
                this.props.onLayout(this.data.FormLayout[this.state.layoutCur])
            })
        }
    }
    next() {
        if (this.state.layoutCur + 1 === this.data.FormLayout.length) {
            const layoutEndForm = {
                "_id": this.data._id,
                "title": this.data.title,
                "description": this.data.description,
                "owner": this.data.owner,
                "FormField": [
                    {
                        "_id": this.data._id,
                        "type": "string",
                        "titleDis": "Cảm ơn bạn đã sử dụng Dynamic form",
                        "component": "TitleField",
                        "description": "Chúc bạn sử dụng phần mềm vui vẻ ^^",
                        "owner": this.data.owner,
                    },
                ]
            }
            this.props.onLayout(layoutEndForm)
            this.setState({ "submitForm": true })
        } else {
            this.setState({ "layoutCur": this.state.layoutCur + 1 }, () => {
                this.props.onLayout(this.data.FormLayout[this.state.layoutCur])
            })
        }
    }
    render() {
        if (this.state.isLoading) {
            return (
                <div>Form Is Loading ...</div>
            )
        }
        if (this.state.submitForm) {
            return (
                <ViewForm previous={this.previous} mode={2} idForm={this.data._id}/>
            )
        }
        return (
            <ViewForm next={this.next} previous={this.previous} mode={1} />
        )
    }
}

const mapStateToProps = (state) => {
    return {
    }
}
const mapDispatchToProps = (dispatch, props) => {
    return {
        onLayout: (layout) => {
            dispatch(createAction(TYPE.READ_LAYOUT, layout))
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Link) 