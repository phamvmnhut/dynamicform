import React, { Component } from 'react'

import {connect} from 'react-redux'

import * as FormAction from '../Redux/Action/form'

import Header from '../Components/Home/header'

import LayoutMana from '../Components/FormEdit/LayoutMana'
import FieldTab from '../Components/FormEdit/FieldTab'
import ViewForm from '../Components/FormEdit/ViewForm'

class FormEdit extends Component {
    componentDidMount() {
        const id = this.props.match.params.idForm
        this.props.getForm(id)
    }
    render() {
        return (
            <div className="wrap_FormEdit">
                <div className="wrap_header">
                    <Header />
                </div>
                <div className="body_FormEdit">
                    <div className="FormEdit_FieldTab" >
                        <FieldTab />
                    </div>
                    <div className="FormEdit_ViewForm">
                        <ViewForm />
                    </div>
                    <div className="FormEdit_LayoutMana">
                        <LayoutMana />
                    </div>
                </div>
            </div>
        )
    }
}

const mapDispatchToProps = (dispatch, props) => {
    return {
        getForm : (idForm) => {
            dispatch(FormAction.readForm(idForm))
        }
    }
}
export default connect(null, mapDispatchToProps)(FormEdit)