import React, { Component } from 'react'

import Introduce from '../Components/Home/Introduce'
import Feature from '../Components/Home/Feature'
import Customer from '../Components/Home/Customer'
import UsingNow from '../Components/Home/UsingNow'
import Contact from '../Components/Home/Contact'
import Footer from '../Components/Home/footer'

export default class Home extends Component {
    render() {
        return (
            <div className="wrap_home">
                <Introduce />
                <Feature />
                <Customer />
                <UsingNow />
                <Contact />
                <Footer />
            </div>
        )
    }
}
