import * as TYPE from "../Action/type";

let initialState = {
    
}
const LayoutReducer = (state = initialState, action) => {
    switch (action.type) {
        case TYPE.READ_LAYOUT: {
            state = action.payload
            return { ...state }
        }
        case TYPE.RELOAD_LAYOUT: {
            return { ...state }
        }
        case TYPE.UPDATE_LAYOUT: {
            state = action.payload
            return { ...state }
        }
        case TYPE.DELETE_LAYOUT: {
            state = {}
            return { ...state }
        }
        case TYPE.ADD_FIELD: {
            let newState = state
            newState.FormField.push(action.payload)
            return {...newState}
        }
        case TYPE.DELETE_FIELD: {
            let newState = state
            let i =0
            for (i =0; i< state.FormField.length;i++ ) {
                if (state.FormField[i]._id === action.payload) break;
            }
            newState.FormField.splice(i, 1)
            return {...newState}
        }
        case TYPE.UPDATE_FIELD: {
            let newState = state
            let i = 0
            for (i =0; i< state.FormField.length;i++ ) {
                if (state.FormField[i]._id === action.payload._id) break;
            }
            newState.FormField[i] = action.payload
            return {...newState}
        }
        default: return state;
    }
}
export default LayoutReducer