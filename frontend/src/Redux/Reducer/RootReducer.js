import { combineReducers } from 'redux';
import FormReducer from './Form';
import LayoutReducer from './Layout'

const RootReducers = combineReducers({
    form: FormReducer,
    layout: LayoutReducer,
})
export default RootReducers