import * as TYPE from "../Action/type";

let initialState = {
    _id: '',
    title: '',
    description: '',
    FormLayout: [],
    collaborator: [],
    userOwner: ''
}
const FormReducer = (state = initialState, action) => {
    switch (action.type) {
        case TYPE.READ_FORM: {        
            state = action.payload
            return { ...state }
        }
        case TYPE.ADD_LAYOUT: {
            let newState = state
            newState.FormLayout.push(action.payload._id)
            return {...newState}
        }
        case TYPE.UPDATE_FORM: {        
            state = action.payload
            return { ...state }
        }
        case TYPE.DELETE_LAYOUT: {
            let newState = state
            newState.FormLayout.splice(state.FormLayout.indexOf(action.payload),1 )
            return {...newState}
        }
        case TYPE.DELETE_FORM: {        
            state = {}
            return { ...state }
        }
        default: return state;
    }
}
export default FormReducer
