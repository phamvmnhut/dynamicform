import { layoutService } from "../../Services"
import { createAction } from "."
import * as TYPE from "./type"

export const fetchLayout = (IdForm, _id) => {
    return dispatch => {
        layoutService
            .readFull(IdForm, _id)
            .then(res => {
                dispatch(createAction(TYPE.READ_LAYOUT, res.data.data))
            })
            .catch(err => {
                console.log(err);
            })
    }
}

export const addLayout = (IdForm, _id) => {
    return dispatch => {
        const data = {idLayout : _id}
        layoutService
        .createById(IdForm, data)
        .then(res => {
            dispatch(createAction(TYPE.ADD_LAYOUT, res.data.data))
        })
        .catch(err => {
            console.log(err)
        })
    }
}

export const updateLayout = (IdForm, _id, doc) => {
    return dispatch => {
        layoutService
            .updateOne(IdForm, _id, doc)
            .then(res => {
                dispatch(createAction(TYPE.UPDATE_LAYOUT, res.data.data))
            })
            .catch(err => {
                dispatch(createAction(TYPE.RELOAD_LAYOUT))
                console.log('[update Err]', err);
            })
    }
}

export const deleteLayout = (IdForm, _id) => {
    return dispatch => {
        layoutService
            .deleteOne(IdForm, _id)
            .then(res => {
                dispatch(createAction(TYPE.DELETE_LAYOUT, _id))
            })
            .catch(err => {
                console.log(err);
            })
    }
}