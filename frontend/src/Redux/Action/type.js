// user
export const LOGIN_IN = "LOGIN_IN"
export const REG_USER = "REG_USER"
export const FETCH_INFO = "FETCH_INFO"
export const REFRESH_TOKEN = "REFRESH_TOKEN"
export const LOG_OUT = "LOG_OUT"

// form 
export const READ_FORM = "READ_FORM"
export const UPDATE_FORM = "UPDATE_FORM"
export const DELETE_FORM = "DELETE_FORM"
export const ADD_LAYOUT = "ADD_LAYOUT"

// layout
export const READ_LAYOUT = "READ_LAYOUT"
export const RELOAD_LAYOUT = "RELOAD_LAYOUT"
export const UPDATE_LAYOUT = "UPDATE_LAYOUT"
export const DELETE_LAYOUT = "DELETE_LAYOUT"
export const ADD_FIELD = "ADD_FIELD"
export const DELETE_FIELD = "DELETE_FIELD"

// field
export const READ_FIELD = "READ_FIELD"
export const UPDATE_FIELD = "UPDATE_FIELD"

