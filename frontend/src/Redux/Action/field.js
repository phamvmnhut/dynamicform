import { fieldService } from "../../Services"
import { createAction } from "."
import * as TYPE from "./type"

export const fetchField = (IdForm, IdLayout, _id) => {
    return dispatch => {
        fieldService
            .readOne(IdForm, IdLayout, _id)
            .then(res => {
                dispatch(createAction(TYPE.READ_FIELD, res.data.data))
            })
            .catch(err => {
                console.log(err);
            })
    }
}

export const updateField = (IdForm, IdLayout, _id, doc) => {
    return dispatch => {
        fieldService
            .updateOne(IdForm, IdLayout, _id, doc)
            .then(res => {
                dispatch(createAction(TYPE.UPDATE_FIELD, res.data.data))
            })
            .catch(err => {
                console.log(err);
            })
    }
}

export const deleteField = (IdForm, IdLayout, _id) => {
    return dispatch => {
        fieldService
            .deleteOne(IdForm, IdLayout, _id)
            .then(res => {
                dispatch(createAction(TYPE.DELETE_FIELD, _id))
            })
            .catch(err => {
                console.log(err);
            })
    }
}

export const addField = (IdForm, IdLayout, _id) => {
    return dispatch => {
        const data = {idField : _id}
        fieldService
        .createById(IdForm, IdLayout, data)
        .then(res => {
            dispatch(createAction(TYPE.ADD_FIELD, res.data.data))
        })
        .catch(err => {
            console.log(err)
        })
    }
}