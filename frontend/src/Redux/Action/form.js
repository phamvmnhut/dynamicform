import { formService } from "../../Services"
import { createAction } from "."
import * as TYPE from "./type"

export const readForm = (_id) => {
    return dispatch => {
        formService
            .readOne(_id)
            .then(res => {
                dispatch(createAction(TYPE.READ_FORM, res.data.data))
            })
            .catch(err => {
                console.log(err);
            })
    }
}

export const updateForm = (_id, doc) => {
    return dispatch => {
        formService
            .updateOne(_id, doc)
            .then(res => {
                dispatch(createAction(TYPE.UPDATE_FORM, res.data.data))
            })
            .catch(err => {
                console.log(err);
            })
    }
}

export const deleteForm = (_idForm) => {
    return dispatch => {
        formService
            .deleteOne(_idForm)
            .then(res => {
                dispatch(createAction(TYPE.DELETE_FORM, res.data.data))
            })
            .catch(err => {
                console.log(err);
            })
    }
}